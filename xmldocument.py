"""
	XMLDocumentReader

	This is a top down reader for an XML Document.  Though it can
	also be used in an HTML document, so long as the document is perfectly balanced.
	Otherwise it will fail to read it.

	This generates a tree from the XML and then enables use of commands to
	browse through the tree to find the data you are interested in.

	This is not likely the most efficient type of parser available, but it
	does make it easier if you are doing a one off type of coding.

	You will be able to use references such as:

				- root
	[9]             - anonymous node 9
	[9]#            - list of attributes from anonymous node 9
	[9]#NAME        - attribute "NAME" from anonymous node 9
	LISTNODE        - node named LISTNODE off the root node
	LISTNODE/NAME   - Node named NAME. By default if there is more than one
				  Element named the same in the same level, then this will
				  give you the first element.
	LISTNODE[]  - List of the nodes with this name at the current level.
	LISTNODE[1] - gives the second (all indexes start at 0) of the nodes
				  named at this level.
	LISTNODE#NAME   - Retrieves the attribute named "NAME" from the node named
				  "LISTNODE" the # identifier must only be used as the end
				  of a path.
	LISTNODE$       - Retrieves the entire text contained only within LISTNODE.
	LISTNODE$[] - Retrieves the entire text as list of sections.
	LISTNODE$[2]    - Retrieves 2nd section of text in LISTNODE.

	../[]           - Retrieves sibling nodes of the current node.
	..          - Retrieves the parent node of this node.

	These basically do the same work you would otherwise do, using the
	XMLNode objects.

"""

import codecs
import xml.sax.saxutils as sax


def repeatstr(c,count):
	result=""
	while(count>0):
		result=result+c
		count-=1
	return result


class       XMLNode:
	def __init__(self,parent,name,attributes):
		self.attributes=attributes
		self.parent=parent
		self.name=name
		self.children=[]
		self.textlist=[]
		self.newchild=None

	def getChild(self,childidentifier=None):
		return None

	def getParent(self):
		return None

	def pathname(self):
		r=""
		p=self
		while (p):
			r="/"+p.name+r
			p=p.parent
		return r
	
	def __str__(self):
		# Write return the result of all children as XML text.
		return self.asText(0)

	def asText(self, level, encoding=None):
		result=u""
		result=repeatstr("\t",level)+"<"+self.name
		for attr in self.attributes.keys():
			if (self.attributes[attr]==None):
				result+=" "+attr
			else:
				result+=" "+attr+'="'+self.attributes[attr]+'"'
		if (len(self.children)):
			result+=">\n"
			for child in self.children:
				result+=child.asText(level+1, encoding)
			result+=repeatstr("\t",level)+"</"+self.name+">\n"
		else:
			if (len(self.textlist)>0):
				result+=">"
				for line in self.textlist:
					line=sax.escape(line)
					if (line!=None):
						if (len(self.textlist)>1):
							result+=line+"\n"
						else:
							if (encoding!=None):
								result+=line
							else:
								result+=line
				if (not len(self.textlist)>1):
					result+="</"+self.name+">\n"
				else:
					result+="\n"+repeatstr("\t",level)+"</"+self.name+">\n"
			else:
				result+="/>\n"
		return result

	def addElement(self,name):
		newelement=XMLNode(self,name,{})
		self.children.append(newelement)
		return newelement

	def remove(self,child=None):
		if (child==None):
			if (self.parent != None):
				self.parent.remove(self)
		elif (type(child)==type(self)):
			try:
				self.children.remove(child)
				return True
			except:
				pass
		elif (type(child)==type(0)):
			try:
				del self.children[child]
				return True
			except:
				pass
		elif (type(child)==type("")):
			for a in self.children:
				if (child==a.name):
					self.remove(a)
					return True
		return False

	def text(self):
		if self.textlist != None:
			return self.textlist[0]
		return ""

	def setText(self,text):
		if (not len(self.children)):
			self.textlist=[text]

	def setAttribute(self,name,value):
		self.attributes[name]=value

	def removeAttribute(self,name):
		if (self.attributes.has_key(name)):
			del self.attributes[name]

	def locate(self,path):
		import string
		pathlist=string.split(path,"/")
		if (pathlist == None):
			return None
		#Path defines the parent of this object
		if (pathlist[0]==".."):
			if (len(pathlist)>1):
				return self.parent.locate(string.join(pathlist[1:],"/"))
			else:
				return self.parent
		ourpath=pathlist[0]
		if (len(pathlist)>1):
			path=string.join(pathlist[1:],"/")
		else:
			path=""

		searchresult=self
		searchname=""
		counter=0
		while (counter < len(ourpath)):
			if ((ourpath[counter]=="#" or ourpath[counter]=="$") and counter > 0):
				for child in self.children:
					if (child.name==searchname):
						return child.locate(ourpath[counter:])
			if (ourpath[counter]=="#" or ourpath[counter]=="$"):
				if (searchresult != self):
					path=ourpath[counter:]+path
					return searchresult.locate(path)

				if (ourpath[counter]=="#"):
					if (counter+1 < len(ourpath)):
						name=ourpath[counter+1:]
						try:
							return self.attributes[name]
						except:
							return None
					else:
						return self.attributes
				else:
					if (counter ==len(ourpath)-1):
						return string.join(self.textlist," ")
					else: # following [] [#] are allowed
						import re
						pobj=re.compile("\[(.*)\]")
						mobj=pobj.match(ourpath[counter+1:])
						if (mobj == None):
							raise ValueError ("only [] and [#], may follow a $")
						else:
							index=-1
							if (mobj.group(1)):
								index=string.atoi(mobj.group(1))
							if (index==-1):
								return self.textlist
							else:
								try:
									return self.textlist[index]
								except:
									raise ValueError ("no text fragment of the index %d" %(index))
			if (ourpath[counter]=="["):
				index=-1
				indextext=""
				counter+=1
				while(counter < len(ourpath)):
					if (ourpath[counter]=="]"):
						if (indextext):
							index=string.atoi(indextext)
						break
					indextext=indextext+ourpath[counter]
					counter=counter+1
				if (index==-1):
					if (searchname==""):
						return self.children
					rlist=[]
					for child in self.children:
						if (child.name==searchname):
							rlist.append(child)
					return rlist
				else:
					if (searchname==""):
						searchresult=self.children[index]
						counter+=1
						continue
					else:
						rlist=[]
						for child in self.children:
							if (child.name==searchname):
								rlist.append(child)
						searchresult=rlist[index]
						searchname=""
						counter+=1
						continue

			searchname=searchname+ourpath[counter]
			counter=counter+1

		if (searchname != ""):
			for child in self.children:
				if (child.name==searchname):
					if (path==""):
						return child
					else:
						return child.locate(path)
		else:
			if (path==""):
				return searchresult
			else:
				return searchresult.locate(path)

"""
	def start_element(self,name,attrs):
		print "start - %s"%(name)
		if (self.newchild == None):
			self.newchild=XMLNode(self,name,attrs)
		else:
			self.newchild.start_element(name,attrs)

	def end_element(self,name):
		print "end - %s"%(name)
		if (self.newchild != None):
			if (self.newchild.newchild != None):
				self.newchild.end_element(name)
			if (self.newchild.name == name):
				self.children.append(self.newchild)
				self.newchild=None

	def     char_data(self,data):
		print data
		if (self.newchild == None):
			self.textlist.append(data)
			all=""
			for item in self.textlist:
				all=all+item
			self.textlist=[all]
		else:
			self.newchild.char_data(data)
"""

class       XMLDocument(XMLNode):
	def __str__(self):
		result=u""
		encoding=""
		if (self.encoding!=None):
			encoding='encoding="%s"'%(self.encoding)
		result+='<?xml  version="1.0" %s ?>\n'%(encoding)

		if (not self.children[0]==None):
			result+=self.children[0].asText(0,self.encoding)
		return result

	def __init__(self,text,encoding=None):
		self.encoding=encoding
		self.objlist=[]
		XMLNode.__init__(self,None,"root",{})
		if (text=="" or text.strip()==""):
			raise "Empty text, unable to parse."
		self.text=text
		self.InitializeFromText(text)

	def SaveToFile(self, filepath):
		f=None
		if (self.encoding!=None):
			f=codecs.open(filepath,"w",self.encoding)
		else:
			f=open(filepath,"w")
		f.write(self.__str__())
		f.close()

	def InitializeFromText(self,text):
		self.objlist=[self]
	
		import xml.parsers.expat
		if (self.encoding != None):
			self.xmlparser=xml.parsers.expat.ParserCreate(self.encoding)
		else:
			self.xmlparser=xml.parsers.expat.ParserCreate()
		self.xmlparser.StartElementHandler=self.start_element
		self.xmlparser.EndElementHandler=self.end_element
		self.xmlparser.CharacterDataHandler=self.char_data
		self.xmlparser.Parse(text)
		return None
	
	def start_element(self,name,attrs):
		obj = self.objlist[-1]
		newchild=XMLNode(self,name,attrs)
		self.objlist.append(newchild)
		obj.children.append(newchild)

	def end_element(self,name):
		self.objlist=self.objlist[:-1]
		
	def     char_data(self,data):
		obj = self.objlist[-1]

		obj.textlist.append(data)
		all=""
		for item in obj.textlist:
			all=all+item
		obj.textlist=[all]


def DocumentFromFile(filepath,encoding=None):
	f=None
	if (encoding==None):
		f=open(filepath,"r")
	else:
		f=codecs.open(filepath,"r",encoding)
	data=f.read()
	f.close()
	d=XMLDocument(data,encoding)
	return d

if __name__=="__main__":
	x=DocumentFromFile("test.xml")
	y=x.locate("/default/a/b/c$")
	x.locate("/default/a/b").addElement("d").setText(y)

	counter=0
	for c in y:
		x.locate("/default/a/b").addElement("d%d"%(counter)).setText(c)

	x.encoding="utf-8"
	x.SaveToFile("testout.xml")
