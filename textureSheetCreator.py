
from bisect import insort
import os
import sys
import xmldocument
import subprocess
import tempfile
import plistlib
import array
import __builtin__
import logging
from PIL import Image
from PIL import ImageDraw

from optparse import OptionParser

# globals
logger = None


class Point(object):
	def __init__(self, x, y):
		self.x = x
		self.y = y
	def __repr__(self):
		return self.__class__.__name__ + "(%s, %s)" % (self.x, self.y)
	def __eq__(self, other):
		if (other == None):
			return False
		return self.x == other.x and self.y == other.y


class Rectangle(object):
	__slots__ = ["x", "y", "sizex", "sizey"]
	def __init__(self, x, y, sizex, sizey):
		self.x = x
		self.y = y
		self.sizex = sizex
		self.sizey = sizey

	def intersects(self, other):
		return other.x < self.x + self.sizex and self.x < other.x + other.sizex and \
			   other.y < self.y + self.sizey and self.y < other.y + other.sizey


class RectanglePacker(object):
	class Anchor(Point):
		def __cmp__(self, other):
			if (other == None):
				return False

			return self.x + self.y > other.x + other.y


	def __init__(self, areax, areay):
		"""
		areax, areay: maximum width,height of the packing area.
		"""
		self._max_areax = areax
		self._max_areay = areay

		# Rectangles contained in the packing area
		self._rectangles = []

		# Anchoring points where new rectangles can potentially be placed
		self._anchors = [self.Anchor(0, 0)]

		# Current width,height of the packing area
		self._areax = 1
		self._areay = 1

	def setMax(self,x,y):

		self._max_areax = x
		self._max_areay = y

	def pack(self, rect_sizex, rect_sizey):
		"""
		Tries to allocate space for a rectangle in the packing area.
		rect_sizex,rect_sizey: width,height of the rectangle to allocate.
		returns: an Anchor that represents the placement, if a space can be found.
		  Or None if no placement can be found.
		"""
		# Try to find an anchor where the rectangle fits in, enlarging the packing
		# area and repeating the search recursively until it fits or the
		# maximum allowed size is exceeded.
		anchor_idx,canflip = self._select_anchor(rect_sizex, rect_sizey, self._areax, self._areay)

		# No anchor could be found at which the rectangle did fit in
		if anchor_idx == -1:
			return (None,False)

		placement = self._anchors[anchor_idx]

		# Move the rectangle either to the left or to the top until it collides with
		# a neightbouring rectangle. This is done to combat the effect of lining up
		# rectangles with gaps to the left or top of them because the anchor that
		# would allow placement there has been blocked by another rectangle
		flipped=self._optimize_placement(placement, rect_sizex, rect_sizey,canflip)
		if (flipped):
			x=rect_sizex
			y=rect_sizey
			rect_sizey=x
			rect_sizex=y

		# Remove the used anchor and add new anchors at the upper right and lower left
		# positions of the new rectangle

		# The anchor is only removed if the placement optimization didn't
		# move the rectangle so far that the anchor isn't blocked anymore
		if ((placement.x + rect_sizex) > self._anchors[anchor_idx].x) and \
		   ((placement.y + rect_sizey) > self._anchors[anchor_idx].y):
			del self._anchors[anchor_idx]

		# Add new anchors at the upper right and lower left coordinates of the rectangle
		insort(self._anchors, self.Anchor(placement.x + rect_sizex, placement.y))
		insort(self._anchors, self.Anchor(placement.x, placement.y + rect_sizey))

		# Finally, we can add the rectangle to our packed rectangles list
		self._rectangles.append(Rectangle(placement.x, placement.y, rect_sizex, rect_sizey))

		return (placement,flipped)


	def density(self):
		"""Return the current covered density [0,1] of the enclosing rectangle."""
		tot = sum((r.sizex * r.sizey for r in self._rectangles), 0.0)
		return tot / (self._max_areax * self._max_areay)


	def _optimize_placement(self, placement, rect_sizex, rect_sizey, canflip):
		"""
		Optimizes the rectangle's placement by moving it either left or up to fill
		any gaps resulting from rectangles blocking the anchors of the most optimal
		placements.

		placement: Placement to be optimized.
		rect_sizex,rect_sizey: width,height of the rectangle to be optimized.
		"""
		origin=(placement.x,placement.y)

		rlist=[]
		if (canflip):
			rlist=[Rectangle(placement.x, placement.y, rect_sizex, rect_sizey),
				   Rectangle(placement.x, placement.y, rect_sizey, rect_sizex)
			]
		else:
			rlist=[Rectangle(placement.x, placement.y, rect_sizex, rect_sizey)]			

		winningindex=0

		for rect in rlist:
			# Try in the normal orientation first
			# Try to move the rectangle to the left as far as possible
			finished_x=0
			while self._is_free(rect, self._max_areax, self._max_areay):
				rect.x -= 1
				finished_x=rect.x

			# Reset rectangle to original position
			rect.x = placement.x

			# Try to move the rectangle upwards as far as possible
			while self._is_free(rect, self._max_areax, self._max_areay):
				rect.y -= 1

			rect.x=finished_x

		top_most_rect=None
		left_most_rect=None
		top_most=placement.y
		left_most=placement.x

		for rect in rlist:
			if (rect.x<=left_most):
				left_most_rect=rect
			if (rect.y<=top_most):
				top_most_rect=rect

		result_rect=None
		if (top_most <= left_most):
			placement.y=top_most
			result_rect=top_most_rect
		else:
			placement.x=left_most
			result_rect=left_most_rect

		if (rlist.index(result_rect)==1):
			return True
		else:
			return False



	def _select_anchor(self, rect_sizex, rect_sizey, total_packing_areax, total_packing_areay):
		"""
		Searches for a free anchor and recursively enlarges the packing area
		if none can be found.

		rect_sizex,rect_sizey: width,height of the rectangle to be placed.
		total_packing_areax,total_packing_areax: width,height of the tested packing area.
		Return: ondex of the anchor the rectangle is to be placed at or -1 if the rectangle
		does not fit in the packing area anymore.
		"""
		# Try to locate an anchor powhere the rectangle fits in
		free_anchor_idx,canflip = self._find_anchor(rect_sizex, rect_sizey,
											total_packing_areax, total_packing_areay)

		# If a the rectangle fits without resizing packing area (any further in case
		# of a recursive call), take over the new packing area size and return the
		# anchor at which the rectangle can be placed.
		if free_anchor_idx != -1:
			self._areax = total_packing_areax
			self._areay = total_packing_areay
			return free_anchor_idx,canflip

		# If we reach self.point, the rectangle did not fit in the current packing
		# area and our only choice is to try and enlarge the packing area.

		# For readability, determine whether the packing area can be enlarged
		# any further in its width and in its height
		can_enlargex = total_packing_areax < self._max_areax
		can_enlargey = total_packing_areay < self._max_areay
		should_enlargey = (not can_enlargex) or (total_packing_areay < total_packing_areax)

		# Try to enlarge the smaller of the two dimensions first (unless the smaller
		# dimension is already at its maximum size). 'shouldEnlargeHeight' is True
		# when the height was the smaller dimension or when the width is maxed out.
		if can_enlargey and should_enlargey:
			# Try to double the height of the packing area
			return self._select_anchor(rect_sizex, rect_sizey, total_packing_areax,
									   min(total_packing_areay * 2, self._max_areay))
		elif can_enlargex:
			# Try to double the width of the packing area
			return self._select_anchor(rect_sizex, rect_sizey,
									   min(total_packing_areax * 2, self._max_areax),
									   total_packing_areay)
		else:
			# Both dimensions are at their maximum sizes and the rectangle still
			# didn't fit. We give up!
			return -1,False


	def _find_anchor(self, rect_sizex, rect_sizey, total_packing_areax, total_packing_areay):
		"""
		Locates the first free anchor at which the rectangle fits.

		rect_sizex,rect_sizey: width,height of the rectangle to be placed.
		total_packing_areax,total_packing_areax: total width,height of the packing area.
		returns: the index of the first free anchor or -1 if none is found.
		"""
		possible_pos = Rectangle(0, 0, rect_sizex, rect_sizey)

		# Walk over all anchors (which are ordered by their distance to the
		# upper left corner of the packing area) until one is discovered that
		# can house the new rectangle.
		self_anchors = self._anchors
		self_is_free = self._is_free

		# See if the rectangle would fit in at self.anchor point
		canflip=False
		firstindex=-1

		for i in xrange(len(self._anchors)): # low level loop for Psyco
			possible_pos.x = self_anchors[i].x
			possible_pos.y = self_anchors[i].y
			possible_pos.sizex=rect_sizex
			possible_pos.sizey=rect_sizey

			if self_is_free(possible_pos, total_packing_areax, total_packing_areay):
				possible_pos.sizex=rect_sizey
				possible_pos.sizey=rect_sizex
				if self_is_free(possible_pos,total_packing_areax, total_packing_areay):
					canflip=True
				firstindex=i
				break

		if (firstindex==-1):
			return -1,False

		# In the optimal case return right away
		if (canflip and firstindex>-1):
			return firstindex,canflip

		secondindex=-1

		for i in xrange(len(self._anchors)): # low level loop for Psyco
			possible_pos.x = self_anchors[i].x
			possible_pos.y = self_anchors[i].y
			possible_pos.sizex=rect_sizex
			possible_pos.sizey=rect_sizey

			if self_is_free(possible_pos, total_packing_areax, total_packing_areay):
				possible_pos.sizex=rect_sizey
				possible_pos.sizey=rect_sizex
				if self_is_free(possible_pos,total_packing_areax, total_packing_areay):
					canflip=True
					secondindex=i
					break

		if (secondindex>-1):
			return secondindex,True

		if (firstindex>-1):
			return firstindex,False

		# No anchor points were found where the rectangle would fit in
		return -1,False


	def _is_free(self, rect, total_packing_areax, total_packing_areay):
		"""
		Determines whether the rectangle can be placed in the packing area
		at its current location.

		rect: Rectangle whose position to check.
		total_packing_areax,total_packing_areax: total width,height of the packing area.
		returns: True if the rectangle can be placed at its current position.
		"""
		# If the rectangle is partially or completely outside of the packing
		# area, it can't be placed at its current location
		if (rect.x < 0) or (rect.y < 0) or \
		   ((rect.x + rect.sizex) > total_packing_areax) or \
		   ((rect.y + rect.sizey) > total_packing_areay):
			return False

		# Brute-force search whether the rectangle touches any of the other
		# rectangles already in the packing area
		#return not any(r.intersects(rect) for r in self._rectangles) #slower
		for r in self._rectangles:
			if r.intersects(rect):
				return False

		# Success! The rectangle is inside the packing area and doesn't overlap
		# with any other rectangles that have already been packed.
		return True


def dopack(options,args):
	outputfilepath = args[0]
	allinputpaths = args[1:]
	rects=[]
	sprites=[]
	spritesarea=0
	p=None
	reduced=float(options.reduce)

	allfiles=[]

	# Gather all files first.
	for inputfilepath in allinputpaths:
		# Find the height,width of all the specified files.
		if (os.path.isfile(inputfilepath)):
			allfiles.append(os.path.abspath(inputfilepath))
			continue

		for root,dirs,files in os.walk(inputfilepath,True):
			if ".svn" in dirs:
				dirs.remove(".svn")

			for f in files:
				fpath=os.path.join(root,f)
				ext=f.split(".")[-1]
				if (ext.lower()!="png"):
					continue

				allfiles.append(fpath)

	#Now calculate the rectangle for each image, in both trimmed and full size.
	for fpath in allfiles:
		img=Image.open(fpath)
		img.load()

		print img.size

		imgs=img.split()[-1]
		index=0
		rows=[]
		cols=[]
		d=list(imgs.getdata())
		# dice up into rows

		for index in range(0,img.size[1]):
			rows.append(d[index*img.size[0] : (index*img.size[0])+img.size[0]])
		
		for x in range(0,img.size[0]):
			cols.append([])
		for x in range(0,img.size[0]):
			for y in range(0,img.size[1]):
				p=d[x+(y*img.size[0])]
				cols[x].append(p)

		b=[0,0,0,0]
		e=[0,0,0,0]

		# Find the top row with a pixel greater than 0
		found=0
		index=0
		notsolid=0
		for row in rows:
			if (found):
				if (notsolid):
					if b[1] > 0:
						b[1]=found-1
						e[1]=1
					break
			for p in row:
				if (p!=0):
					if (found==0):
						#print row
						b[1]=index
						found=index
					if (notsolid==0):
						if (p<255):
							notsolid=1
			index+=1


		# Find the bottom row with a pixel greater than 0
		found=0
		index=0
		notsolid=0
		rows.reverse()
		for row in rows:
			if (found):
				if (notsolid):
					if b[3] < img.size[1]:
						index -=1
						b[3]=img.size[1]-found
						e[3]=1
					break
			for p in row:
				if (p!=0):
					if (found==0):
						#print row
						b[3]=img.size[1]-index
						found=index
					if (notsolid==0):
						if (p<255):
							notsolid=1
			index+=1

		# Find the column from the left
		found=0
		index=0
		notsolid=0
		for col in cols:
			if (found):
				if (notsolid):
					if b[0] > 0:
						b[0]=found-1
						e[0]=1
					break
			for p in col:
				if (p!=0):
					if (found==0):
						#print col
						b[0]=index
						found=index
					if (notsolid==0):
						if (p<255):
							notsolid=1
			index+=1
		
		# Find the column from the right
		found=0
		index=0
		notsolid=0
		cols.reverse()
		for col in cols:
			if (found):
				if (notsolid):
					if b[2] < img.size[0]:
						index -=1
						b[2]=img.size[0]-found
						e[2]=1
					break
			for p in col:
				if (p!=0):
					if (found==0):
						#print col
						b[2]=img.size[0]-index
						found=index
					if (notsolid==0):
						if (p<255):
							notsolid=1

			index+=1

		logger.info("%s --> softedges=%d,%d,%d,%d"% (os.path.basename(fpath),round(e[0]),round(e[1]),round(e[2]),round(e[3])))	
		b[2]+=1
		b[3]+=1
		logger.info("%s --> rect=%d,%d,%d,%d"% (os.path.basename(fpath),round(b[0]),round(b[1]),round(b[2]),round(b[3])))
		orig_w=int(round(img.size[0]) * reduced)
		orig_h=int(round(img.size[1]) * reduced)
		trim_w=int(round((b[2]-b[0])) * reduced)
		trim_h=int(round((b[3]-b[1])) * reduced)
		imgb=img.crop(b)

		imgb_origin=(b[0]+int(round(trim_w/2)),  b[1]+int(round(trim_h/2)) )

		if (reduced < 1.0):
			imgb=imgb.resize((int(trim_w),int(trim_h)),Image.ANTIALIAS)

		imgb.filename=img.filename

		orig_center=(int(((orig_w*1.0)/2)),int(((orig_h*1.0)/2)))
		trim_center=imgb_origin

		offsetx=orig_center[0]-trim_center[0]
		offsety=orig_center[1]-trim_center[1]

		logger.info("%s --> orig=%d,%d  trim=%d,%d  offset=%d,%d" %(os.path.basename(fpath),orig_center[0],orig_center[1],trim_center[0],trim_center[1],offsetx,offsety))

		offset=(offsetx,offsety)

		if (options.autopadding):
			autopadding_x=e[0]+e[2]
			autopadding_y=e[1]+e[3]
		else:
			autopadding_x=0
			autopadding_y=0


		colorrect=(int(b[0]*reduced),int(b[1]*reduced), int((b[2]-b[0])*reduced),int((b[3]-b[1])*reduced) ) 
		r= (trim_w+options.padding+autopadding_x,trim_h+options.padding+autopadding_y,imgb,orig_w,orig_h,colorrect,offset)

		rects.append(r)
		spritesarea+=(trim_w+options.padding+autopadding_x)*(trim_h+options.padding+autopadding_y)

		logger.info("%s --> basetrim=%d,%d  autotrim=%d,%d" %(os.path.basename(fpath),trim_w+options.padding,trim_h+options.padding,trim_w+options.padding+autopadding_x,trim_h+options.padding+autopadding_y))

	# Sort all rectangles, biggest,widest first
	# Then put those which will be flipped into the image first.
	sortedrects=[]
	
	# Area Sort
	if options.sort=="area":
		for r in rects:
			r2=None
			for r2 in sortedrects:
				area_r=r[0]*r[1]
				area_r2=r2[0]*r2[1]
				if (area_r > area_r2):
					index_r2=sortedrects.index(r2)
					sortedrects.insert(index_r2,r)
					break
			else:
				sortedrects.append(r)

	if options.sort=="area_inverted":
		for r in rects:
			r2=None
			for r2 in sortedrects:
				area_r=r[0]*r[1]
				area_r2=r2[0]*r2[1]
				if (area_r > area_r2):
					index_r2=sortedrects.index(r2)
					sortedrects.insert(index_r2,r)
					break
			else:
				sortedrects.append(r)
		sortedrects.reverse()

	if options.sort=="shortside":
		for r in rects:
			r2=None
			for r2 in sortedrects:
				short=r[0] 
				if (r[1]<short):
					short=r[1]
				short2=r2[0] 
				if (r2[1]<short2):
					short2=r2[1]
				if (short > short2):
					index_r2=sortedrects.index(r2)
					if (index_r2+1 == len(sortedrects)):
						sortedrects.insert(index_r2,r)	
						break				
					continue
				else:
					index_r2=sortedrects.index(r2)
					sortedrects.insert(index_r2,r)
					break
			else:
				sortedrects.append(r)
		#sortedrects.reverse()
	if options.sort=="widestfirst":
		for r in rects:
			r2=None
			for r2 in sortedrects:
				wide=r[0] 
				if (r[1]>wide):
					wide=r[1]
				wide2=r2[0] 
				if (r2[1]>wide):
					wide2=r2[1]
				if (wide >= wide2):
					index_r2=sortedrects.index(r2)
					sortedrects.insert(index_r2,r)
					break
			else:
				sortedrects.append(r)

	rects=sortedrects
	
	sizes=[(256,256),(512,512),(512,1024),(1024,1024),(1024,2048),(2048,2048)]
	startsize=None
	startindex=0
	startsize=sizes[0]

	if (startsize == None):
		logger.error("Could not fit given images in an image less than 2048x2048")
		return 1

	isvertical=True
	mode="normal"
	
	p=RectanglePacker(startsize[0],startsize[1])
	rstart=0
	sprites=[]
	nonfitting=rects
	fitting=[]

	while(mode!="failed" and mode!="succeeded"): 
		if (mode=="normal"):
			newnonfitting=[]
			for r in nonfitting:
				w=r[0]
				h=r[1]

				(position,flipped) = p.pack(w, h)

				if (position):
					if (flipped):
						w=r[1]
						h=r[0]

					fitting.append(r)
					sprites.append((position.x,position.y,w,h,r[2],flipped,r[5],r[3],r[4],r[6]))				
					continue

				newnonfitting.append(r)

			nonfitting=newnonfitting

			print len(nonfitting)

			if (len(nonfitting)):
				mode="grow"
				continue
			mode="succeeded"

		if (mode=="grow"):
			if (startindex<len(sizes)):
				startindex+=1
				startsize=sizes[startindex]
				p.setMax(startsize[0],startsize[1])
				mode="normal"
				logger.info ("Growing from %dx%d -> %dx%d"%(sizes[startindex-1][0],sizes[startindex-1][1], startsize[0],startsize[1]))

				continue
			mode="failed"

		""" Create the image """	
		if (mode=="succeeded"):
			baseimg=Image.new("RGBA",(startsize[0],startsize[1]))
			drawimg=ImageDraw.Draw(baseimg)

			logger.info ("%d x %d"%(startsize[0],startsize[1]))

			datafile={}
			datafile["metadata"]={}
			m=datafile["metadata"]
			m["format"]=2
			m["realTextureFileName"]=os.path.basename(outputfilepath)
			m["size"]="{%d,%d}"%(startsize[0],startsize[1])
			m["textureFileName"]=os.path.basename(outputfilepath)

			datafile["frames"]={}

			for sp in sprites:
				rtext=""
				datafile["frames"][os.path.basename(sp[4].filename)]={}
				frame=datafile["frames"][os.path.basename(sp[4].filename)]

				flipped=sp[5]
				if (flipped):
					frame["frame"]="{{%d,%d},{%d,%d}}"%(sp[0],sp[1],sp[3],sp[2])
				else:
					frame["frame"]="{{%d,%d},{%d,%d}}"%(sp[0],sp[1],sp[2],sp[3])
				frame["rotated"]=flipped
				frame["sourceColorRect"]="{{%d,%d},{%d,%d}}"%(sp[6][0],sp[6][1],sp[6][2],sp[6][3])
				frame["sourceSize"]="{%d,%d}"%(sp[7],sp[8])
				frame["offset"]="{%d,%d}"%(sp[9][0],sp[9][1])

				if (sp[5]==False):
					baseimg.paste(sp[4],(sp[0]+1,sp[1]+1))
				else:
					img=sp[4]
					img=img.rotate(-90)
					baseimg.paste(img,(sp[0]+1,sp[1]+1))

				if (int(options.debug)==1):
					drawimg.setfill(1)
					drawimg.line([(sp[0],sp[1]), (sp[0]+sp[2],sp[1])], fill="#000000")
					drawimg.line([(sp[0],sp[1]), (sp[0],sp[1]+sp[3])], fill="#000000")

			baseimg.save(outputfilepath)

			pfilename=os.path.basename(outputfilepath).split(".")[:-1]
			pfilename.append("plist")
			pfilename=".".join(pfilename)
			pfilepath=os.path.join(os.path.dirname(outputfilepath),pfilename)
			plistlib.writePlist(datafile,pfilepath)

		if (mode=="failed"):
			logger.error("The images were unable to be fit into a rectangle")
			return 1
	return 0


def main():
	global logger
	usage = "Usage: texture_packer [options] <outputfilepath> [<input_directory>[...,<input_directoryN>]]"
	
	if (len(sys.argv) == 1):
		print usage
		return 1

	logformat="%(asctime)-15s ===%(levelname)s=== %(message)s"
	logging.basicConfig(format=logformat)
	logger=logging.getLogger('texturepacker')

	opts = OptionParser(usage=usage)
	opts.add_option("","--verbose",dest="verbose", action="store_true", default=False,help="help display non debug information about the packing")
	opts.add_option("","--reduce",dest="reduce",default="1.0",help="Reduce the individual sprites by this percentage. Used for generating non-retina versions of the same graphics.")
	opts.add_option("","--debug",dest="debug",default="0",help="If the set, then the sprites in the final image will have their debug rect painted in the image.")
	opts.add_option("","--padding",dest="padding",default=0,help="Number of pixels to always include between images in the final sheet.")
	opts.add_option("","--disable_autopadding",dest="autopadding",action="store_false",default=True,help="By default auto padding is enabled, including this kills that option.")
	opts.add_option("","--sort",dest="sort",default="area",help="Rectangle sort function to use. options: [area],shortside ")


	(options,args)=opts.parse_args()
	
	if (options.verbose):
		logger.setLevel(10)

	if (len(args)==0):
		logger.error("No outputfilepath or inputfilepattern specified")
		return 1

	if (len(args)==1):
		logger.error("No inputfilepattern specified")
		return 1

	# validate paths
	e=0
	for a in args[1:]:
		if (not os.path.exists(a)):
			logger.error("The path '%s' was not found."%(a))
			e=1
	if (e):
		return 1

	return dopack(options,args)

if (__name__=="__main__"):
	sys.exit(main())
